import './App.css';
import Header from './Components/Header';
import About from './Components/About';
import Skills from './Components/Skills';
import Footer from './Components/Footer';
import Frase from './Components/Frase';
import Projects from './Components/Projects';

function App() {
  return (
    <>
      <Header />
      <About />
      <Skills />
      <Projects />
      <Frase />
      <Footer />
    </>
  );
}

export default App;
