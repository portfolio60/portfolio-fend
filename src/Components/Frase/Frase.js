import { Container, Row, Col } from 'react-bootstrap';
import './Frase.css';

const Frase = () => {
    return (
        <section id="frase">
            <Container fluid className="frase mt-5">
                <Row className="justify-content-center align-items-center" style={{height:"100%"}}>
                    <Col md={"8"}>
                        <h3 className="frase-text text-center">
                            "Un experto es una persona que ha cometido todos los errores que pueden cometerse en un campo muy reducido"
                        </h3>
                    </Col>
                </Row>
            </Container>
        </section>
    );
}

export default Frase;