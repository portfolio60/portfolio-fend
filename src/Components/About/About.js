import { Container, Row, Col, Alert, Button } from "react-bootstrap";
import './About.css'
import profile from './../../images/profile.jpg'    // Anotar esto importante -> sino no andaba img

const About = () => {
    return (
        <section id="about">
            <Container className="About mt-5">
                <h3 className="text-center">About me</h3>
                <Row className="p-4">
                    <Col xl={"4"}>
                        <img src={profile} className="profile-image" />
                    </Col>
                    <Col xl={"8"}>
                        <Alert variant="dark">
                            <h4>¿Quién soy?</h4>
                            <p>Alohamora wand elf parchment, Wingardium Leviosa hippogriff, house dementors betrayal. Holly, Snape centaur portkey ghost Hermione spell bezoar Scabbers. Peruvian-Night-Powder werewolf, Dobby pear-tickle half-moon-glasses, Knight-Bus. Padfoot snargaluff seeker: Hagrid broomstick mischief managed. Snitch Fluffy rock-cake, 9 ¾ dress robes I must not tell lies. Mudbloods yew pumpkin juice phials Ravenclaw’s Diadem 10 galleons Thieves Downfall.</p>
                            <Button block variant={"secondary"}>
                                Ver mas..
                            </Button>
                        </Alert>
                    </Col>
                </Row>
                <hr />
            </Container>
        </section>

    );
}

export default About;