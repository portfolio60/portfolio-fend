import { Container } from 'react-bootstrap'
import './Header.css'
const Header = () => {
    return (
        <Container fluid className="header text-center pt-5">
            <h2 >Luciano Livramento</h2>
        </Container>
    );
}

export default Header;