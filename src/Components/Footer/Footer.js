import { Row, Col } from 'react-bootstrap';
import './Footer.css'

const Footer = () => {
    return (
        <footer className="container-fluid footer mt-5 pt-3 pb-4">
            <Row className="justify-content-center align-items-center text-center pt-3">
                <Col md={"3"}>
                    <address>Luis Agote 1200</address>
                    <p>Tel: 4278-1234</p>
                    <i class="fab fa-instagram fa-2x text-danger pr-1"></i>
                    <i class="fab fa-facebook-f fa-2x text-primary pr-1"></i>
                    <i class="fab fa-linkedin-in fa-2x text-primary"></i>
                </Col>
                <Col md={"3"}><p>Copyright © 2021 Luciano Livramento</p></Col>
                <Col md={"4"}>
                    {/* aria-hidden="false" */}
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3278.5495668139597!2d-58.254195385047524!3d-34.7417465723208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95a32ef5d5aa1305%3A0x8239b391d88c1977!2sEstadio%20Ciudad%20de%20Quilmes!5e0!3m2!1ses!2sar!4v1609445095337!5m2!1ses!2sar" width={"100%"} height={"200px"} frameBorder={"0"} style={{border:"0"}} allowFullScreen={""} tabIndex={"0"}></iframe>
                </Col>
            </Row>
        </footer>
    );
}

export default Footer;