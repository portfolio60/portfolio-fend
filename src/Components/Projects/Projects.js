import { Row, Col, Card, Button } from 'react-bootstrap';
import './Projects.css'
import boxingMachine from './../../images/boxing-machine.jpg';
import pillbox from './../../images/pillbox.png'

const Projects = () => {
    return (
        <section id="projects" className="container mt-5">
            <h3 className="text-center">Proyectos</h3>
            <Row className="justify-content-center mt-5 mb-3">
                <Col lg={"4"} md={"6"} className="mb-5">
                    <Card className="text-center mb-1 mi-card" bg="dark" text="white">
                        <Card.Img variant="top" src={pillbox} style={{ height: "250px", width: "100%" }} />
                        <Card.Body>
                            <Card.Title>Smart Pillbox</Card.Title>
                            <Card.Text>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Id culpa similique labore tenetur, vitae beatae, adipisci sed hic, sunt maiores dolorum saepe commodi ipsa minus soluta dignissimos? Iure, ullam nihil.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Button block variant="outline-dark" href="https://www.google.com">Visitar sitio web</Button>
                    <Button block variant="light" className="btn-repo">Repositorio Git <i className="fab fa-github" /></Button>
                    {/* <p className="text-center mt-3">Repositorio Git <i className="fab fa-github" /></p> */}
                </Col>
                <Col lg={"4"} md={"6"} className="mb-5">
                    <Card className="text-center mb-1 mi-card" bg="dark" text="white">
                        <Card.Img variant="top" src={boxingMachine} style={{ height: "250px", width: "100%" }} />
                        <Card.Body>
                            <Card.Title>Titan Puncher</Card.Title>
                            <Card.Text>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Id culpa similique labore tenetur, vitae beatae, adipisci sed hic, sunt maiores dolorum saepe commodi ipsa minus soluta dignissimos? Iure, ullam nihil.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Row className="justify-content-center">
                        <Col sm={"6"}>
                            <Button variant="outline-dark" href="https://www.google.com">Visitar sitio web</Button>
                        </Col>
                        <Col sm={"6"}>
                            <Button variant="outline-dark">Repositorio Git <i className="fab fa-github" /></Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </section>
    );
}

export default Projects;