import { Row, Col } from 'react-bootstrap';
import './Skills.css';

const Skills = () => {
    return (
        <section id="skills" className="container skills mt-5">
            <h3 className="text-center">Skills</h3>
            {/* 2 filas, 2 columnas cada una, y 2 subcolumnas internas */}
            {/* Primera fila de 2 skills */}
            {/* Mas adelante hacer un Componente Skill, no repetir toda la estructura para cada uno, pasaje de props, y ver uso 'for' */}
            <Row className="justify-content-center ">
                <Col md={"6"} className="mt-3 mb-3">
                    <Row className="pt-3 pb-3 align-items-center">
                        {/* Logo */}
                        <Col className="d-flex justify-content-center">
                            <i className="fab fa-html5 icono" style={{ color: "orange" }}></i>
                        </Col>
                        {/* Description */}
                        <Col>
                            <p>Some HTML description Lorem ipsum dolor sit, amet consectetur adipisicing elit. Libero, ducimus.</p>
                        </Col>
                    </Row>
                </Col>
                <Col md={"6"} className="mt-3 mb-3">
                    <Row className="pt-3 pb-3 align-items-center">
                        <Col className="d-flex justify-content-center">
                            <i className="fab fa-css3-alt icono" style={{ color: "blue" }}></i>
                        </Col>
                        <Col>
                            <p>Some CSS description Lorem ipsum dolor sit, amet consectetur adipisicing elit. Libero, ducimus.</p>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <hr />
        </section>
    );
}

export default Skills;