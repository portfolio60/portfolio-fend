## Problemas con imagenes
Tratando de usar imagenes tuve problemas para referenciar la url.

### Dentro de JSX (en un componente), por ejemplo:
En una etiqueta `<img>` para colocar el 'src="..."' se debe <b>importar la imagen</b> de la siguiente manera
`import profile from './../../images/profile.jpg'`

Con ese nombre asignado a la importación de la imagen, colocaremos el source:
`<img src={profile} />`


### En CSS
Para aplicar una imagen de fondo, por ejemplo, utilicé url relativa `background-image: url('./../../images/banner.jpg');`

Pero, hay 2 temas con esto:
<ol>
    <li>Las direcciones escritas asi son un quilombo</li>
    <li>
        En la documentación oficial dicen que lo mejor es hacerlo con 'import', como hice en los componetes, pero en CSS dice que simplemente pones la url y listo, incluso la escriben acotada, como si la imagen estuviera guardada en la misma carpeta del componente 🤷‍♀️
        En muchos lugares encontré que se puede direccionar directo a 'public' pero medio se contradecían muchos y los probé y no me andaban (una que no probe y supongo que anda, es utilizando %PUBLIC_URL%, pero en la doc oficial dicen q no es lo mejor hacer eso)
    </li>
</ol>

### Referencias:
- https://create-react-app.dev/docs/adding-images-fonts-and-files/
- https://stackoverflow.com/questions/44796786/how-to-set-a-background-image-in-reactjs
- https://stackoverflow.com/questions/34582405/react-wont-load-local-images


